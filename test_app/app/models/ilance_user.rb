class IlanceUser < ApplicationRecord
	OPERATIONS = ["Recent", "Project", "Username", "Category"]
	has_many :ilance_projects, foreign_key: 'user_id'
end
