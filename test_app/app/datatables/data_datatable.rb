class DataDatatable < AjaxDatatablesRails::ActiveRecord
  delegate :params, :h, to: :@view

  def initialize(view)
    @view = view
    fetch_projects
  end

  def as_json(options = {})
    {
      aaData: data
    }
  end

  private
  def data
    @projects.map do |project|
      [
        project.project_title,
        project.ilance_user.username,
        project.ilance_category.name
      ]
    end
  end
  def fetch_projects
    if params[:sort_by].present?
      case params[:sort_by]
      when "0"
        @projects = IlanceProject.all.order(:date_added)
      when "1"
        @projects = IlanceProject.all.order(:project_title)
      when "2"
        @projects = IlanceProject.joins(:ilance_user).order('ilance_users.username')
      when "3"
        @projects = IlanceProject.joins(:ilance_category).order('ilance_categories.name')
      end
    end
  end
end