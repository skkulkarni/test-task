class ApplicationController < ActionController::Base
    helper_method :current_user

    rescue_from ::ActionController::RoutingError, with: :error_occurred


    def error_occurred(exception)
      flash[:danger] = 'Kindly Try Again'
      redirect_to data_index_path
    end

    def session_authenticate
      redirect_to root_path unless current_user.present?
    end

    def current_user
      @current_user ||= IlanceUser.find(session[:user_id])
    rescue StandardError
      session.delete(:user_id)
      nil
    end

end
