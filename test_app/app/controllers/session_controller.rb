require 'digest/md5'
class SessionController < ApplicationController
  def new
    if current_user
      redirect_to data_path
    else
      flash[:alert] = nil
      render 'login'
    end
  end

  def create
    user = IlanceUser.find_by_username(params[:session][:username])
    params[:session][:password] = Digest::MD5.hexdigest("#{Digest::MD5.hexdigest(params[:session][:password])}#{user.salt}")
    if user && user.password == params[:session][:password]
      session[:user_id] = user.id
      redirect_to data_path, notice: 'Logged in!'
    else
      flash[:alert] = 'Username or password is invalid'
      render 'login'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = 'Logged Out !!'
    redirect_to root_url
  end

  private

  def employee_params
    params.require(:session).permit(:username, :password)
  end
end
