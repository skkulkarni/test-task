Rails.application.routes.draw do
  get 'login', to: 'session#new', as: 'new'
  post 'login', to: 'session#create', as: 'login'
  get 'logout', to: 'session#destroy', as: 'logout'
  resources :data
  root 'session#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
